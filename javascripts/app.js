/* Foundation v2.1.5 http://foundation.zurb.com */
$(document).ready(function () {
  	
  	/****
  	* Get the last known order and set the default for fallback
  	*****/
  	var defaultOrder = new Array();
  	defaultOrder = ["links","weather","twitter","hr","news","translate","calendar","google","todo"];
  	var listOrder = $.jStorage.get("sortable",defaultOrder);
  	
  	/****
  	* Iterate through the blocks and place them into the list in the given order
  	*****/
  	for(n=0;n<listOrder.length;n++)
  	{
  		//alert(listOrder[n]);
  		$("#"+listOrder[n]).appendTo("#sortable");
  	}


  	/****
  	* Make the blocks sortable with the jQuery UI
  	*****/
	$( "#sortable" ).sortable({
		handle: 'h2',
		placeholder: 'ui-state-highlight placeholder',
		tolerance: 'pointer',
		update: function(ev,ui)
		{
			// Every time a block is dropped, save the order
			$.jStorage.set("sortable", $("#sortable").sortable("toArray"));
		}
	});

	/****
  	* Disable the selection of elements in orde to be able to drag
  	*****/
	$( "#sortable" ).disableSelection();

	/****
  	* Clear the saved order and start over.
  	*****/
	$("#flush").click(function(e){
		e.preventDefault();
		$.jStorage.flush();
		window.location.reload();
	});

});
